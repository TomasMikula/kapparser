package kappa.parser

import java.nio.file.{ Files, Paths }

import scala.collection.JavaConversions._
import scala.language.implicitConversions
import scala.util.parsing.input.CharSequenceReader

import org.scalatest.{Matchers, WordSpec}
import org.scalatest.exceptions.TestFailedException

import Parsers._
import kappa.syntax._

class ParsersSpec extends WordSpec with Matchers {

  implicit private class AugmentedParser[A](val p: Parsers.Parser[A]) {
    def matches(input: String): Unit = {
      if(!parseAll(p, input).successful) {
        fail()
      }
    }
  }

  implicit private def stringToReader(s: String) = new CharSequenceReader(s)

  "underscore" should {
    "match an underscore" in {
      underscore("_ abc").successful should be (true)
    }
    "not match at the beginning of a word" in {
      underscore("_abc").successful should be (false)
    }
  }

  "comment" should {
    "end at the end of line" in {
      val res = comment("# this is a comment. \n This is not a comment.")
      res.successful should be (true)
      res.next.first should be ('\n')
    }
  }

  "agentInstruction" should {
    "match '%agent:'" in { agentInstruction matches "%agent:" }
  }

  "agentTypeName" should {
    "match 'A'" in { agentTypeName matches "A" }
    "match 'A-B'" in { agentTypeName matches "A-B" }
  }

  "siteDeclaration" should {
    "match 'a'" in { siteDeclaration matches "a" }
    "match 'abc5'" in { siteDeclaration matches "abc5" }
    "match 'a~b~c'" in { siteDeclaration matches "abc5" }
    "match 'a~0~1'" in { siteDeclaration matches "a~0~1" }
  }

  "sitePattern" should {
    "match 'a'" in { sitePattern matches "a" }
    "match 'a~b'" in { sitePattern matches "a~b" }
    "not match multiple site states" in { a [TestFailedException] should be thrownBy { sitePattern matches "a~b~c" } }
    "match 'a!1'" in { sitePattern matches "a!1" }
    "only match numeric bond identifiers" in { a [TestFailedException] should be thrownBy { sitePattern matches "a!b1" } }
    "match 'a~b!1'" in { sitePattern matches "a~b!1" }
    "match 'a~x!b~y.B'" in { sitePattern matches "a~x!b~y.B" }
  }

  "agentTypeDefinition" should {
    "not match 'A'" in { a [TestFailedException] should be thrownBy { agentTypeDefinition matches "A" } }
    "match 'A(b~0~1, c~u~p, d)'" in { agentTypeDefinition matches "A(b~0~1, c~u~p, d)" }
  }

  "agentTypeDeclaration" should {
    "match '%agent: A(b~0~1, c~u~p, d)'" in {
      agentTypeDeclaration matches "%agent: A(b~0~1, c~u~p, d)"
    }
  }

  "tokenDeclaration" should {
    "match '%token: ca+'" in { tokenDeclaration matches "%token: ca+" }
  }

  "variableDefinition" should {
    """match "%var: 'xyz' 5"""" in { variableDefinition matches "%var: 'xyz' 5" }
    """match "%var: 'xyz' 5.1e-3"""" in { variableDefinition matches "%var: 'xyz' 5.1e-3" }
    """match "%var: 'n5mCG' |DNA(init~mC!_, base~C!_)|"""" in { variableDefinition matches "%var: 'n5mCG' |DNA(init~mC!_, base~C!_)|"}
  }

  "doubleLiteral" should {
    "match '5'" in { doubleLiteral matches "5" }
    "match '.5'" in { doubleLiteral matches ".5" }
    "match '1.1e-3" in { doubleLiteral matches "1.1e-3" }
    "match '5.1E-3" in { doubleLiteral matches "5.1E-3" }
  }

  "numExpr" should {
    "match 7" in { numExpr matches "7" }
    "match 7.7" in { numExpr matches "7.7" }
    "match 'varname'" in { numExpr matches "'varname'" }
    "match INF" in { numExpr matches "INF" }
    "match [pi]" in { numExpr matches "[pi]" }
    "match [E]" in { numExpr matches "[E]" }
    "match [E+]" in { numExpr matches "[E+]" }
    "match [E-]" in { numExpr matches "[E-]" }
    "match [Emax]" in { numExpr matches "[Emax]" }
    "match [T]" in { numExpr matches "[T]" }
    "match [Tsim]" in { numExpr matches "[Tsim]" }
    "match [Tmax]" in { numExpr matches "[Tmax]" }
    "match 1+2*3" in { numExpr matches "1+2*3" }
    "match 1/2-3" in { numExpr matches "1/2-3" }
    "match 5 [mod] 7" in { numExpr matches "5 [mod] 7" }
    "match 2^3" in { numExpr matches "2^3" }
    "match -5 - --3" in { numExpr matches "-5 - -3" }
    "match [log] 7" in { numExpr matches "[log] 7" }
    "match [sin] 7" in { numExpr matches "[sin] 7" }
    "match [cos] 7" in { numExpr matches "[cos] 7" }
    "match [tan] 7" in { numExpr matches "[tan] 7" }
    "match [sqrt] 7" in { numExpr matches "[sqrt] 7" }
    "match [exp] 7" in { numExpr matches "[exp] 7" }
    "match [int] 7" in { numExpr matches "[int] 7" }
    "match [log] [sin] [cos] [tan] [sqrt] 7" in { numExpr matches "[log] [sin] [cos] [tan] [sqrt] 7" }
    "match [log] [sin] [cos] [tan] -[sqrt] 7" in { numExpr matches "[log] [sin] [cos] [tan] -[sqrt] 7" }
    "match |ca2+|" in { numExpr matches "|ca2+|" }
    "match [max] 2 [min] 3 4" in { numExpr matches "[max] 2 [min] 3 4" }
    "match [pi]^-[E]" in { numExpr matches "[pi]^-[E]" }
    "match 3*(4+([max] -5 (-7)))" in { numExpr matches "3*(4+([max] -5 (-7)))" }
  }

  "1.1e-3" should {
    "be parsed as double literal" in {
      parseAll(numExpr, "1.1e-3") match {
        case Success(DoubleLiteral(1.1e-3), _) =>
        case Success(expr, _) => fail("was parsed as " + expr)
        case NoSuccess(msg, _) => fail("was not parsed at all")
      }
    }
  }

  "1+2*3" should {
    "be parsed as 1+(2*3)" in {
      parseAll(numExpr, "1+2*3") match {
        case Success(PlusExpr(IntLit(1), TimesExpr(IntLit(2), IntLit(3))), _) =>
        case Success(expr, _) => fail("was parsed as " + expr)
        case NoSuccess(msg, _) => fail("was not parsed at all")
      }
    }
  }

  "1*2+3" should {
    "be parsed as (1*2)+3" in {
      parseAll(numExpr, "1*2+3") match {
        case Success(PlusExpr(TimesExpr(IntLit(1), IntLit(2)), IntLit(3)), _) =>
        case Success(expr, _) => fail("was parsed as " + expr)
        case NoSuccess(msg, _) => fail("was not parsed at all")
      }
    }
  }

  "3-2-1" should {
    "be parsed as (3-2)-1" in {
      parseAll(numExpr, "3-2-1") match {
        case Success(MinusExpr(MinusExpr(IntLit(3), IntLit(2)), IntLit(1)), _) =>
        case Success(expr, _) => fail("was parsed as " + expr)
        case NoSuccess(msg, _) => fail("was not parsed at all")
      }
    }
  }

  "-[max] [log][min] 5 + -[sin]-7 3 * 4 + 1 [pi]^-[E]" should {
    "be parser as -(max(+(*(log(min(+(5, -(sin(-(7)))), 3)), 4), 1), ^(pi, -([E]))))" in {
      parseAll(numExpr, "-[max] [log][min] 5 + -[sin]-7 3 * 4 + 1 [pi]^-[E]") match {
        case Success(
            UnaryMinusExpr(
                MaxExpr(
                    PlusExpr(
                        TimesExpr(
                            LogExpr(
                                MinExpr(
                                    PlusExpr(
                                        IntLit(5),
                                        UnaryMinusExpr(
                                            SinExpr(
                                                UnaryMinusExpr(
                                                    IntLit(7))))),
                                    IntLit(3))),
                            IntLit(4)),
                        IntLit(1)),
                    PowExpr(
                        Pi(),
                        UnaryMinusExpr(
                            EventCount())))),
            _) =>
        case Success(expr, _) => fail("was parsed as " + expr)
        case NoSuccess(msg, _) => fail("was not parsed at all: " + msg)
      }
    }
  }

  "strExpr" should {
    """match "foo"""" in { strExpr matches "\"foo\"" }
    """match ('a'+5)."foo"""" in { strExpr matches """('a'+5)."foo"""" }
  }

  "boolExpr" should {
    "match [true]" in { boolExpr matches "[true]" }
    "match [false]" in { boolExpr matches "[false]" }
    "match [not][true]" in { boolExpr matches "[not][true]" }
    "match [E]>20" in { boolExpr matches "[E]>20" }
    "match |ca2+| >= 7.5e-9 || [not] 'a' > 100 && [true]" in { boolExpr matches "|ca2+| >= 7.5e-9 || [not] 'a' > 100 && [true]" }
  }

  "initialCondition" should {
    "match '%init: 1000 (B(), R(i~0, x))'" in { initialCondition matches "%init: 1000 (B(), R(i~0, x))" }
    "match '%init: ca2+ <- 7.9e-3'" in { initialCondition matches "%init: ca2+ <- 7.9e-3" }
  }

  "plotDirective" should {
    """match "%plot: 'var_name'"""" in { plotDirective matches "%plot: 'var_name'" }
  }

  "observableDefinition" should {
    """match "%obs: 'bound R' |R(i!_)|"""" in { observableDefinition matches "%obs: 'bound R' |R(i!_)|" }
    """match "%obs: 'A.B' |A(a!1),B(b!1)|"""" in { observableDefinition matches "%obs: 'A.B' |A(a!1),B(b!1)|" }
    """match "%obs: 'n_c' 0"""" in { observableDefinition matches "%obs: 'n_c' 0" }
  }

  "agentPattern" should {
    "not match 'A'" in { a [TestFailedException] should be thrownBy { agentPattern matches "A" } }
  }

  "rule" should {
    """match "A(s, t!5, u!_, v?, w~u!1), B() -> C(c~p!_)"""" in { rule matches "A(s, t!5, u!_, v?, w~u!1), B() -> C(c~p!_)" }
    """match "'name' A() -> B()"""" in { rule matches "'name' A() -> B()" }
    """match "A() -> B() @.4"""" in { rule matches "A() -> B() @.4" }
    """match "A() -> B() @ 'variable'"""" in { rule matches "A() -> B() @ 'variable'" }
    """match "'name' A() -> B() @.4"""" in { rule matches "'name' A() -> B() @.4" }
    """match "A() <-> B()"""" in { rule matches "A() <-> B()" }
    """not match "A() <-> B() @ 2"""" in { a [TestFailedException] should be thrownBy { rule matches "A() <-> B() @ 2" } }
    """match "A() <-> B() @ 2, 3"""" in { rule matches "A() <-> B() @ 2, 3" }
    """match "A() | 0.39:ca+ + 1:atp -> 6:adp"""" in { rule matches "A() | 0.39:ca+ + 1:atp -> 6:adp" }
    """match "A() -> B() @ 'k2'('k1')"""" in { rule matches "A() -> B() @ 'k2'('k1')" }
    """match "A() <-> B() @ 'k2'('k1'), 1(2)"""" in { rule matches "A() <-> B() @ 'k2'('k1'), 1(2)" }
    """match " -> A()"""" in { rule matches " -> A()" }
    """match "A() -> """" in { rule matches "A() -> " }
    """match "A-B() -> C-D()"""" in { rule matches "A-B() -> C-D()" }
  }

  "definition" should {
    """match '%def: "displayCompression" "none" "weak" "strong"'""" in { definition matches """%def: "displayCompression" "none" "weak" "strong"""" }
  }

  "effect" should {
    "match $ADD [E]/2 A()" in { effect matches "$ADD [E] A()" }
    "match $DEL [T]+1.3 A()" in { effect matches "$DEL [T]+1.3 A()" }
    "match ca2+ <- |ca2+|/2" in { effect matches "ca2+ <- |ca2+|/2" }
    "match $SNAPSHOT" in { effect matches "$SNAPSHOT" }
    """match $SNAPSHOT "a/b/c"""" in { effect matches """$SNAPSHOT "a/b/c"""" }
    "match $STOP" in { effect matches "$STOP" }
    """match $STOP "a/b/c"""" in { effect matches """$STOP "a/b/c"""" }
    "match $FLUX [true]" in { effect matches "$FLUX [true]" }
    """match $FLUX "a"."/"."b" [false]""" in { effect matches """$FLUX "a"."/"."b" [false]""" }
    "match $TRACK 'x' [true]" in { effect matches "$TRACK 'x' [true]" }
    "match $UPDATE 'x' 'x' [mod] 'a'" in { effect matches "$UPDATE 'x' 'x' [mod] 'a'" }
    """match $PRINT "'a'*5 = ".('a'*5)""" in { effect matches """$PRINT "'a'*5 = ".('a'*5)""" }
    """match $PRINTF "a"."/"."b" 5+6"""" in { effect matches """$PRINTF "a"."/"."b" 5+6""" }
  }

  "perturbation" should {
    "match %mod: [T]>10 do ($TRACK 'Cpp' [true] ; $UPDATE 'cflow' 'Cpp')" in {
      perturbation matches "%mod: [T]>10 do ($TRACK 'Cpp' [true] ; $UPDATE 'cflow' 'Cpp')"
    }
    "match %mod: [T]>10 do \\\n ($TRACK 'Cpp' [true] ; \\\n $UPDATE 'cflow' 'Cpp')" in {
      perturbation matches "%mod: [T]>10 do \\\n ($TRACK 'Cpp' [true] ; \\\n $UPDATE 'cflow' 'Cpp')"
    }
    """match %mod: repeat 'a'>10 do $DEL 'a'/2 A() ; $PRINT "# of A() = ".('a'/2) until [E]>20""" in {
      perturbation matches """%mod: repeat 'a'>10 do $DEL 'a'/2 A() ; $PRINT "# of A() = ".('a'/2) until [E]>20"""
    }
    """match %mod: [T]>2 do ($UPDATE 'k_deam' 10000000000000 ; $PRINT <".">)""" in {
      perturbation matches """%mod: [T]>2 do ($UPDATE 'k_deam' 10000000000000 ; $PRINT <".">)"""
    }
  }

  "kappaFile" should {
    "accept a sample kappa file" in {
      val path = Paths.get(classOf[ParsersSpec].getResource("abc-pert.ka").toURI())
      val input = Files.readAllLines(path) mkString "\n"
      kappaFile matches input
    }
  }
}