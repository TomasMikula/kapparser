package kappa.syntax

import scala.util.parsing.input.Reader

sealed trait SourceRange {
  def start: Int
  def   end: Int
  def isDefined: Boolean
}
object SourceRange {

  private case class SourceRangeImpl(start: Int, end: Int) extends SourceRange {
    override def isDefined = true
  }

  case object UndefinedSourceRange extends SourceRange {
    override def start = throw new NoSuchElementException("Undefined source range")
    override def end   = throw new NoSuchElementException("Undefined source range")
    override def isDefined = false
  }

  def apply(start: Int, end: Int): SourceRange = {
    if(start < 0) throw new IllegalArgumentException(s"Start must not be negative: $start")
    if(end < start) throw new IllegalArgumentException(s"End must not be less than start: [$start, $end)")
    SourceRangeImpl(start, end)
  }

  def apply(input1: Reader[_], input2: Reader[_]): SourceRange =
    apply(input1.offset, input2.offset)
}

sealed trait SyntacticElement {
  var sourceRange: SourceRange = SourceRange.UndefinedSourceRange
  val children: Seq[SyntacticElement]
}

trait LeafSyntacticElement extends SyntacticElement {
  val children = Seq.empty
}

abstract class Parent(override val children: SyntacticElement*) extends SyntacticElement {
  def this(elems: List[SyntacticElement]) = this(elems: _*)
}

case class KappaFile(declarations: List[Declaration]) extends Parent(declarations)

/** Superclass for all top-level syntactic elements. */
trait Declaration extends SyntacticElement


case class Comment() extends Declaration with LeafSyntacticElement

case class AgentTypeDeclaration(
    inst: AgentInstruction,
    body: AgentTypeDefinition)
  extends Parent(inst, body) with Declaration

case class TokenDeclaration(
    inst: TokenInstruction,
    name: TokenName)
  extends Parent(inst, name) with Declaration

sealed trait Rule extends Declaration {
  val name: Option[RuleName]
  val lhsAgents: KappaExpr
  val lhsTokens: List[QuantifiedToken]
  val rhsAgents: KappaExpr
  val rhsTokens: List[QuantifiedToken]
  val rate: Option[RuleRate]

  override lazy val children = name.toList:::lhsAgents::lhsTokens:::rhsAgents::rhsTokens:::rate.toList
}

case class ForwardRule(
    name: Option[RuleName],
    lhsAgents: KappaExpr,
    lhsTokens: List[QuantifiedToken],
    rhsAgents: KappaExpr,
    rhsTokens: List[QuantifiedToken],
    rate: Option[ForwardRate])
  extends Rule

case class BidiRule(
    name: Option[RuleName],
    lhsAgents: KappaExpr,
    lhsTokens: List[QuantifiedToken],
    rhsAgents: KappaExpr,
    rhsTokens: List[QuantifiedToken],
    rate: Option[BidiRate])
  extends Rule

case class QuantifiedToken(
    coeff: NumExpr,
    name: TokenName)
  extends Parent(coeff, name)

case class VariableDefinition(
    inst: VarInstruction,
    name: VariableName,
    value: NumExpr)
  extends Parent(inst, name, value) with Declaration

trait InitialCondition extends Declaration

case class InitialAgents(
    inst: InitInstruction,
    multiplicity: NumExpr,
    species: SpeciesList)
  extends Parent(inst, multiplicity, species) with InitialCondition

case class InitialToken(
    inst: InitInstruction,
    token: TokenName,
    conc: NumExpr)
  extends Parent(inst, token, conc) with InitialCondition

case class PlotDirective(
    inst: PlotInstruction,
    expr: NumExpr)
  extends Parent(inst, expr) with Declaration

case class ObservableDefinition(
    inst: ObsInstruction,
    name: VariableName,
    expr: NumExpr)
  extends Parent(inst, name, expr) with Declaration

case class Definition(
    inst: DefInstruction,
    name: String,
    values: List[String])
  extends Parent(inst) with Declaration

trait Perturbation extends Declaration

case class OneShotPerturbation(
    inst: ModInstruction,
    cond: BoolExpr,
    doKw: DoKw,
    effects: List[Effect])
  extends Parent(inst::cond::doKw::effects) with Perturbation

case class RepeatedPerturbation(
    inst: ModInstruction,
    repeatKw: RepeatKw,
    cond: BoolExpr,
    doKw: DoKw,
    effects: List[Effect],
    untilKw: UntilKw,
    stopCond: BoolExpr)
  extends Parent((inst::repeatKw::cond::doKw::effects) :+ untilKw :+ stopCond) with Perturbation

trait Effect extends SyntacticElement

case class AddAgents(
    cmd: AddCmd,
    multiplicity: NumExpr,
    species: SpeciesList)
  extends Parent(cmd, multiplicity, species) with Effect

case class DelAgents(
    cmd: DelCmd,
    multiplicity: NumExpr,
    pattern: AgentPattern)
  extends Parent(cmd, multiplicity, pattern) with Effect

case class SetToken(
    token: TokenName,
    conc: NumExpr)
  extends Effect {
  lazy val children = Seq(token, conc)
}

case class Snapshot(
    cmd: SnapshotCmd,
    file: Option[StrExpr])
  extends Parent(cmd::file.toList) with Effect

case class Stop(
    cmd: StopCmd,
    file: Option[StrExpr])
  extends Parent(cmd::file.toList) with Effect

case class Flux(
    cmd: FluxCmd,
    file: Option[StrExpr],
    toggle: BoolExpr)
  extends Parent((cmd::file.toList) :+ toggle) with Effect

case class Track(
    cmd: TrackCmd,
    varName: VariableName,
    toggle: BoolExpr)
  extends Parent(cmd, varName, toggle) with Effect

case class Update(
    cmd: UpdateCmd,
    varName: VariableName,
    value: NumExpr)
  extends Parent(cmd, varName, value) with Effect

case class Print(
    cmd: PrintCmd,
    s: StrExpr)
  extends Parent(cmd, s) with Effect

case class Printf(
    cmd: PrintfCmd,
    file: StrExpr,
    s: StrExpr)
  extends Parent(cmd, file, s) with Effect

case class PlotEntry(cmd: PlotEntryCmd) extends Parent(cmd) with Effect


case class SpeciesList(species: List[AgentPattern])
  extends Parent(species)

case class AgentTypeDefinition(
    name: AgentTypeName,
    sites: List[SiteDeclaration])
  extends Parent(name::sites)

case class KappaExpr(agents: List[AgentPattern])
extends Parent(agents:_*)

case class AgentPattern(
    typeName: AgentTypeName,
    sites: List[SitePattern])
  extends Parent(typeName::sites)


case class SiteDeclaration(
    name: SiteName,
    states: List[SiteState])
  extends Parent(name::states)

case class SitePattern(
    name: SiteName,
    state: Option[SiteState],
    bond: Option[Bond])
  extends Parent(name::state.toList:::bond.toList) {
  def this(name: SiteName, state: Option[SiteState]) = this(name, state, None)
}

class UnboundSitePattern(
    override val name: SiteName,
    override val state: Option[SiteState])
  extends SitePattern(name, state, None)


case class SiteState(op: Tilde, name: SiteStateName) extends Parent(op, name)


trait Bond extends SyntacticElement

case class NamedBond(op: ExclamationMark, name: BondName)
  extends Parent(op, name) with Bond

case class PatternBond(
    op: ExclamationMark,
    sitePattern: UnboundSitePattern,
    agentTypeName: AgentTypeName)
  extends Parent(op, sitePattern, agentTypeName) with Bond

case class AnyBond(op: ExclamationMark, placeholder: Underscore)
  extends Parent(op, placeholder) with Bond

case class WildcardBond(op: QuestionMark) extends Parent(op) with Bond

sealed trait RuleRate extends SyntacticElement
case class ForwardRate(op: AtSign, rate: RateExpr) extends Parent(op, rate) with RuleRate
case class BidiRate(op: AtSign, rate1: RateExpr, rate2: RateExpr) extends Parent(op, rate1, rate2) with RuleRate
case class RateExpr(primary: NumExpr, uniMolecular: Option[NumExpr]) extends Parent(primary::uniMolecular.toList)


case class AgentTypeName(name: String) extends LeafSyntacticElement
case class TokenName(name: String) extends LeafSyntacticElement
case class SiteName(name: String) extends LeafSyntacticElement
case class BondName(name: String) extends LeafSyntacticElement
case class SiteStateName(name: String) extends LeafSyntacticElement
case class RuleName(name: String) extends LeafSyntacticElement
case class VariableName(name: String) extends LeafSyntacticElement


case class Tilde() extends LeafSyntacticElement
case class Underscore() extends LeafSyntacticElement
case class ExclamationMark() extends LeafSyntacticElement
case class QuestionMark() extends LeafSyntacticElement
case class AtSign() extends LeafSyntacticElement


abstract class Instruction extends LeafSyntacticElement
case class AgentInstruction() extends Instruction
case class TokenInstruction() extends Instruction
case class VarInstruction() extends Instruction
case class PlotInstruction() extends Instruction
case class ObsInstruction() extends Instruction
case class InitInstruction() extends Instruction
case class DefInstruction() extends Instruction
case class ModInstruction() extends Instruction


abstract class Keyword extends LeafSyntacticElement
case class DoKw() extends Keyword
case class RepeatKw() extends Keyword
case class UntilKw() extends Keyword

abstract class PertCmd extends LeafSyntacticElement
case class AddCmd() extends PertCmd
case class DelCmd() extends PertCmd
case class SnapshotCmd() extends PertCmd
case class StopCmd() extends PertCmd
case class FluxCmd() extends PertCmd
case class TrackCmd() extends PertCmd
case class UpdateCmd() extends PertCmd
case class PrintCmd() extends PertCmd
case class PrintfCmd() extends PertCmd
case class PlotEntryCmd() extends PertCmd


trait NumExpr extends StrExpr

case class IntegerLiteral(value: BigInt) extends NumExpr with LeafSyntacticElement
object IntLit { // for pattern matching on Ints
  def unapply(lit: IntegerLiteral): Option[Int] = Some(lit.value.toInt)
}
case class DoubleLiteral(value: Double) extends NumExpr with LeafSyntacticElement

case class VariableRef(name: String) extends NumExpr with LeafSyntacticElement

case class Infinity() extends NumExpr with LeafSyntacticElement
case class Pi() extends NumExpr with LeafSyntacticElement
case class EventCount() extends NumExpr with LeafSyntacticElement
case class ProdEventCount() extends NumExpr with LeafSyntacticElement
case class NullEventCount() extends NumExpr with LeafSyntacticElement
case class EventLimit() extends NumExpr with LeafSyntacticElement
case class BioTime() extends NumExpr with LeafSyntacticElement
case class CpuTime() extends NumExpr with LeafSyntacticElement
case class BioTimeLimit() extends NumExpr with LeafSyntacticElement

abstract class BiNumOperator(f: (NumExpr, NumExpr) => BiNumExpr) extends LeafSyntacticElement {
  def apply(left: NumExpr, right: NumExpr): BiNumExpr = f(left, right)
}
case class PlusOp() extends BiNumOperator(PlusExpr(_, _))
case class MinusOp() extends BiNumOperator(MinusExpr(_, _))
case class TimesOp() extends BiNumOperator(TimesExpr(_, _))
case class DivOp() extends BiNumOperator(DivExpr(_, _))
case class ModOp() extends BiNumOperator(ModExpr(_, _))
case class PowOp() extends BiNumOperator(PowExpr(_, _))
case class MinOp() extends BiNumOperator(MinExpr(_, _))
case class MaxOp() extends BiNumOperator(MaxExpr(_, _))

abstract class BiNumExpr(a: NumExpr, b: NumExpr) extends Parent(a, b) with NumExpr

case class PlusExpr(a: NumExpr, b: NumExpr) extends BiNumExpr(a, b)
case class MinusExpr(a: NumExpr, b: NumExpr) extends BiNumExpr(a, b)
case class TimesExpr(a: NumExpr, b: NumExpr) extends BiNumExpr(a, b)
case class DivExpr(a: NumExpr, b: NumExpr) extends BiNumExpr(a, b)
case class ModExpr(a: NumExpr, b: NumExpr) extends BiNumExpr(a, b)
case class PowExpr(a: NumExpr, b: NumExpr) extends BiNumExpr(a, b)
case class MinExpr(a: NumExpr, b: NumExpr) extends BiNumExpr(a, b)
case class MaxExpr(a: NumExpr, b: NumExpr) extends BiNumExpr(a, b)

abstract class UniNumOperator(f: NumExpr => UniNumOperation) extends LeafSyntacticElement {
  def apply(arg: NumExpr): UniNumOperation = f(arg)
}
case class UnaryMinusOp() extends UniNumOperator(UnaryMinusExpr(_))
case class LogOp() extends UniNumOperator(LogExpr(_))
case class SinOp() extends UniNumOperator(SinExpr(_))
case class CosOp() extends UniNumOperator(CosExpr(_))
case class TanOp() extends UniNumOperator(TanExpr(_))
case class SqrtOp() extends UniNumOperator(SqrtExpr(_))
case class ExpOp() extends UniNumOperator(ExpExpr(_))
case class FloorOp() extends UniNumOperator(FloorExpr(_))

abstract class UniNumOperation(arg: NumExpr) extends Parent(arg) with NumExpr

case class UnaryMinusExpr(arg: NumExpr) extends UniNumOperation(arg)
case class LogExpr(arg: NumExpr) extends UniNumOperation(arg)
case class SinExpr(arg: NumExpr) extends UniNumOperation(arg)
case class CosExpr(arg: NumExpr) extends UniNumOperation(arg)
case class TanExpr(arg: NumExpr) extends UniNumOperation(arg)
case class SqrtExpr(arg: NumExpr) extends UniNumOperation(arg)
case class ExpExpr(arg: NumExpr) extends UniNumOperation(arg)
case class FloorExpr(arg: NumExpr) extends UniNumOperation(arg)

case class TokenConcentration(token: TokenName) extends Parent(token) with NumExpr
case class PatternConcentration(pattern: KappaExpr) extends Parent(pattern) with NumExpr


trait BoolExpr extends SyntacticElement

abstract class CmpExpr(a: NumExpr, b: NumExpr) extends Parent(a, b) with BoolExpr
case class LtExpr(a: NumExpr, b: NumExpr) extends CmpExpr(a, b)
case class LteExpr(a: NumExpr, b: NumExpr) extends CmpExpr(a, b)
case class EqExpr(a: NumExpr, b: NumExpr) extends CmpExpr(a, b)
case class GteExpr(a: NumExpr, b: NumExpr) extends CmpExpr(a, b)
case class GtExpr(a: NumExpr, b: NumExpr) extends CmpExpr(a, b)

abstract class BiBoolExpr(a: BoolExpr, b: BoolExpr) extends Parent(a, b) with BoolExpr
case class OrExpr(a: BoolExpr, b: BoolExpr) extends BiBoolExpr(a, b)
case class AndExpr(a: BoolExpr, b: BoolExpr) extends BiBoolExpr(a, b)

abstract class UniBoolExpr(arg: BoolExpr) extends Parent(arg) with BoolExpr
case class NotExpr(arg: BoolExpr) extends UniBoolExpr(arg)

case class True() extends BoolExpr with LeafSyntacticElement
case class False() extends BoolExpr with LeafSyntacticElement


trait StrExpr extends SyntacticElement

case class StringLiteral(value: String) extends StrExpr with LeafSyntacticElement
case class Concat(a: StrExpr, b: StrExpr) extends Parent(a, b) with StrExpr