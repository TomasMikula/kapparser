package kappa.parser

import kappa.syntax._

import scala.util.parsing.combinator.JavaTokenParsers

object Parsers extends JavaTokenParsers {

  // treat backslash ('\') at the end of line as whitespace
  override protected val whiteSpace = """(\s|\\\r\n|\\\r|\\\n)+""".r

  implicit class RangedParser[T <: SyntacticElement](p: => Parser[T]) extends Parser[T] {
    override def apply(in: Input) = {
      val offset = in.offset
      val start = handleWhiteSpace(in.source, offset)
      val in1 = in.drop(start - offset)
      p(in1) match {
        case s @ Success(t, in2) => t.sourceRange = SourceRange(in1, in2); s
        case noSuccess => noSuccess
      }
    }
  }

  def kappaFile: RangedParser[KappaFile] = directive.* ^^ { KappaFile(_) }

  def directive: Parser[Declaration] = comment | agentTypeDeclaration | tokenDeclaration | variableDefinition | initialCondition | observableDefinition | plotDirective | definition | perturbation | rule

  def comment: RangedParser[Comment] = "#.*".r ^^^ Comment() // '.' does not match line terminators by default

  def agentTypeDeclaration: RangedParser[AgentTypeDeclaration] = agentInstruction ~ agentTypeDefinition ^^ { case i~d => AgentTypeDeclaration(i, d) }

  def tokenDeclaration: RangedParser[TokenDeclaration] = tokenInstruction ~ tokenName ^^ { case i~n => TokenDeclaration(i, n) }

  def variableDefinition: RangedParser[VariableDefinition] = varInstruction ~ variableName ~ numExpr ^^ { case i~n~v => VariableDefinition(i, n, v) }

  def initialCondition: Parser[InitialCondition] = initialAgents | initialToken
  def initialAgents: RangedParser[InitialAgents] = initInstruction ~ numExpr ~ speciesList ^^ { case i~m~s => InitialAgents(i, m, s) }
  def initialToken: RangedParser[InitialToken] = initInstruction ~ tokenName ~ "<-" ~ numExpr ^^ { case i~n~_~c => InitialToken(i, n, c) }

  def plotDirective: RangedParser[PlotDirective] = plotInstruction ~ numExpr ^^ { case i~n => PlotDirective(i, n) }
  def observableDefinition: RangedParser[ObservableDefinition] = obsInstruction ~ variableName ~ numExpr ^^ { case i~n~v => ObservableDefinition(i, n, v) }

  def definition: RangedParser[Definition] = defInstruction ~ stringLiteral ~ stringLiteral.+ ^^ { case i~n~v => Definition(i, n, v) }

  def perturbation: Parser[Perturbation] = oneShotPerturbation | repeatedPerturbation
  def oneShotPerturbation: RangedParser[OneShotPerturbation] =
    modInstruction ~ conditionalEffects ^^ { case i~((c, d, e)) => OneShotPerturbation(i, c, d, e) }
  def repeatedPerturbation: RangedParser[RepeatedPerturbation] =
    modInstruction ~ repeatKw ~ conditionalEffects ~ untilKw ~ boolExpr ^^ { case i~r~((c, d, e))~u~s => RepeatedPerturbation(i, r, c, d, e, u, s) }
  def conditionalEffects: Parser[(BoolExpr, DoKw, List[Effect])] =
    "(" ~> conditionalEffects <~ ")" |
    boolExpr ~ doKw ~ effectList ^^ { case c~k~e => (c, k, e) }
  def effectList: Parser[List[Effect]] =
    "(" ~> effectList <~ ")" |
    rep1sep(effect, ";")
  def effect: Parser[Effect] = addAgents | delAgents | setToken | snapshot | stop | flux | track | update | printf | print | plotEntry
  def addAgents: RangedParser[AddAgents] = addCmd ~ numExpr ~ speciesList ^^ { case a~n~l => AddAgents(a, n, l) }
  def delAgents: RangedParser[DelAgents] = delCmd ~ numExpr ~ agentPattern ^^ { case d~n~p => DelAgents(d, n, p) }
  def setToken: RangedParser[SetToken] = tokenName ~ "<-" ~ numExpr ^^ { case t~_~n => SetToken(t, n) }
  def snapshot: RangedParser[Snapshot] = snapshotCmd ~ strExpr.? ^^ { case s~f => Snapshot(s, f) }
  def stop: RangedParser[Stop] = stopCmd ~ strExpr.? ^^ { case s~f => Stop(s, f) }
  def flux: RangedParser[Flux] = fluxCmd ~ strExpr.? ~ boolExpr ^^ { case x~f~t => Flux(x, f, t) }
  def track: RangedParser[Track] = trackCmd ~ variableName ~ boolExpr ^^ { case t~v~e => Track(t, v, e) }
  def update: RangedParser[Update] = updateCmd ~ variableName ~ numExpr ^^ { case u~v~e => Update(u, v, e) }
  def printf: RangedParser[Printf] = printfCmd ~ strExpr ~ strExpr ^^ { case p~f~s => Printf(p, f, s) }
  def print: RangedParser[Print] = printCmd ~ strExpr ^^ { case p~s => Print(p, s) }
  def plotEntry: RangedParser[PlotEntry] = plotEntryCmd ^^ { PlotEntry(_) }

  def rule: Parser[Rule] = forwardRule | bidiRule
  def forwardRule: RangedParser[Rule] = ruleName.? ~ ruleSide ~ "->" ~ ruleSide ~ forwardRate.? ^^ { case n~((la, lt))~_~((ra, rt))~r => ForwardRule(n, la, lt, ra, rt, r) }
  def bidiRule: RangedParser[Rule] = ruleName.? ~ ruleSide ~ "<->" ~ ruleSide ~ bidiRate.? ^^ { case n~((la, lt))~_~((ra, rt))~r => BidiRule(n, la, lt, ra, rt, r) }
  def ruleSide: Parser[(KappaExpr, List[QuantifiedToken])] =
    kappaExpr ~ "|" ~ rep1sep(quantifiedToken, "+") ^^ { case k~_~t => (k, t) } | // hybrid rule
    rep1sep(quantifiedToken, "+") ^^ { (KappaExpr(Nil), _) } | // token rule
    kappaExpr ^^ { (_, Nil) } // pure rule
  def kappaExpr: RangedParser[KappaExpr] = repsep(agentPattern, ",") ^^ { KappaExpr(_) }

  def speciesList: RangedParser[SpeciesList] =
    "(" ~> nakedSpeciesList <~ ")" | nakedSpeciesList

  def nakedSpeciesList: Parser[SpeciesList] = rep1sep(agentPattern, ",") ^^ { SpeciesList(_) }

  def agentTypeDefinition: RangedParser[AgentTypeDefinition] = agentTypeName ~ ("(" ~> repsep(siteDeclaration, ",") <~ ")") ^^ { case name~sites => AgentTypeDefinition(name, sites) }
  def agentPattern: RangedParser[AgentPattern] = agentTypeName ~ ("(" ~> repsep(sitePattern, ",") <~ ")") ^^ {
    case name~sites => AgentPattern(name, sites)
  }

  def quantifiedToken: RangedParser[QuantifiedToken] = numExpr ~ ":" ~ tokenName ^^ { case e~_~n => QuantifiedToken(e, n) }

  def siteDeclaration: RangedParser[SiteDeclaration] = siteName ~ siteState.* ^^ { case name~states => SiteDeclaration(name, states) }
  def sitePattern: RangedParser[SitePattern] = siteName ~ siteState.? ~ siteBinding.? ^^ { case n~s~b => SitePattern(n, s, b) }
  def unboundSitePattern: RangedParser[UnboundSitePattern] = siteName ~ siteState.? ^^ { case n~s => new UnboundSitePattern(n, s) }

  def siteBinding: Parser[Bond] = anyBond | namedBond | patternBond | wildcardBond
  def namedBond: RangedParser[NamedBond] = exclamationMark ~ bondName ^^ { case e~n => NamedBond(e, n) }
  def anyBond: RangedParser[AnyBond] = exclamationMark ~ underscore ^^ { case e~u => AnyBond(e, u) }
  def patternBond: RangedParser[PatternBond] = exclamationMark ~ unboundSitePattern ~ "." ~ agentTypeName ^^ { case e~s~_~a => PatternBond(e, s, a) }
  def wildcardBond: RangedParser[WildcardBond] = questionMark ^^ { WildcardBond(_) }

  def siteState: RangedParser[SiteState] = tilde ~ siteStateName ^^ { case op~name => SiteState(op, name) }

  def forwardRate: RangedParser[ForwardRate] = atSign ~ rateExpr ^^ { case a~r => ForwardRate(a, r) }
  def bidiRate: RangedParser[BidiRate] = atSign ~ rateExpr ~ "," ~ rateExpr ^^ { case a~fr~_~br => BidiRate(a, fr, br) }
  def rateExpr: RangedParser[RateExpr] = numExpr ~ ("("~>numExpr<~")").? ^^ { case bi~uni => RateExpr(bi, uni) }

  def variableName: RangedParser[VariableName] = "'" ~> "[^']+".r <~ "'" ^^ { VariableName(_) }
  def ruleName: RangedParser[RuleName] = "'" ~> "[^']*".r <~ "'" ^^ { RuleName(_) }
  def agentTypeName: RangedParser[AgentTypeName] = "[a-zA-Z][a-zA-Z0-9_+-]*".r ^^ { AgentTypeName(_) }
  def tokenName: RangedParser[TokenName] = "[a-zA-Z][a-zA-Z0-9_+-]*".r ^^ { TokenName(_) }
  def siteName: RangedParser[SiteName] = "[a-zA-Z][a-zA-Z0-9_+-]*".r ^^ { SiteName(_) }
  def siteStateName: RangedParser[SiteStateName] = """\p{Alnum}+""".r ^^ { SiteStateName(_) }
  def bondName: RangedParser[BondName] = """[0-9]+""".r ^^ { BondName(_) }

  def agentInstruction: RangedParser[AgentInstruction] = "%agent" ~ ":" ^^^ AgentInstruction()
  def tokenInstruction: RangedParser[TokenInstruction] = "%token" ~ ":" ^^^ TokenInstruction()
  def varInstruction: RangedParser[VarInstruction] = "%var" ~ ":" ^^^ VarInstruction()
  def initInstruction: RangedParser[InitInstruction] = "%init" ~ ":" ^^^ InitInstruction()
  def plotInstruction: RangedParser[PlotInstruction] = "%plot" ~ ":" ^^^ PlotInstruction()
  def obsInstruction: RangedParser[ObsInstruction] = "%obs" ~ ":" ^^^ ObsInstruction()
  def defInstruction: RangedParser[DefInstruction] = "%def" ~ ":" ^^^ DefInstruction()
  def modInstruction: RangedParser[ModInstruction] = "%mod" ~ ":" ^^^ ModInstruction()

  def numExpr: Parser[NumExpr] = priority1Expr
  def priority1Expr: RangedParser[NumExpr] = p1Operand ~ (binaryOpP1 ~ prefixedP2Expr).* ^^ { case e~rest => buildBinaryExpr(e, rest) }
  def p1Operand: RangedParser[NumExpr] =
    unaryOpP1 ~ prefixedP2Expr ^^ { case op~e => op(e) } |
    prefixBinaryOpP1 ~ numExpr ~ prefixedP2Expr ^^ { case op~a~b => op(a, b) } |
    priority2Expr
  def prefixedP2Expr: RangedParser[NumExpr] =
    unaryOp ~ prefixedP2Expr ^^ { case op~e => op(e) } |
    prefixBinaryOp ~ numExpr ~ prefixedP2Expr ^^ { case op~a~b => op(a, b) } |
    priority2Expr
  def priority2Expr: RangedParser[NumExpr] = p2Operand ~ (binaryOpP2 ~ prefixedP3Expr).* ^^ { case e~rest => buildBinaryExpr(e, rest) }
  def p2Operand: RangedParser[NumExpr] =
    unaryOpP2 ~ prefixedP3Expr ^^ { case op~e => op(e) } |
    prefixBinaryOpP2 ~ numExpr ~ prefixedP3Expr ^^ { case op~a~b => op(a, b) } |
    priority3Expr
  def prefixedP3Expr: RangedParser[NumExpr] =
    unaryOp ~ prefixedP3Expr ^^ { case op~e => op(e) } |
    prefixBinaryOp ~ numExpr ~ prefixedP3Expr ^^ { case op~a~b => op(a, b) } |
    priority3Expr
  def priority3Expr: RangedParser[NumExpr] = p3Operand ~ (binaryOpP3 ~ prefixedP4Expr).* ^^ { case e~rest => buildBinaryExpr(e, rest) }
  def p3Operand: RangedParser[NumExpr] =
    unaryOpP3 ~ prefixedP4Expr ^^ { case op~e => op(e) } |
    prefixBinaryOpP3 ~ numExpr ~ prefixedP4Expr ^^ { case op~a~b => op(a, b) } |
    priority4Expr
  def prefixedP4Expr: RangedParser[NumExpr] =
    unaryOp ~ prefixedP4Expr ^^ { case op~e => op(e) } |
    prefixBinaryOp ~ numExpr ~ prefixedP4Expr ^^ { case op~a~b => op(a, b) } |
    priority4Expr
  def priority4Expr: RangedParser[NumExpr] =
    atomicExpr |
    "(" ~> numExpr <~ ")"
  def atomicExpr: Parser[NumExpr] = variableRef | numberLiteral | infinity | pi | builtInVar | tokenConcentration | patternConcentration
  def builtInVar: Parser[NumExpr] =
    eventCount | prodEventCount | nullEventCount | eventLimit |
    bioTime | cpuTime | bioTimeLimit
  def variableRef: RangedParser[VariableRef] = variableName ^^ { case VariableName(name) => VariableRef(name) }
  def numberLiteral: Parser[NumExpr] = doubleLiteral ||| integerLiteral // will return integer literal if both match the same input
  def integerLiteral: RangedParser[IntegerLiteral] = wholeNumber ^^ { x => IntegerLiteral(BigInt(x)) }
  def doubleLiteral: RangedParser[DoubleLiteral] = floatingPointNumber ^^ { x => DoubleLiteral(x.toDouble) }
  def infinity: RangedParser[Infinity] = "INF" ^^^ Infinity()
  def pi: RangedParser[Pi] = "[pi]" ^^^ Pi()
  def eventCount: RangedParser[EventCount] = "[E]" ^^^ EventCount()
  def prodEventCount: RangedParser[ProdEventCount] = "[E+]" ^^^ ProdEventCount()
  def nullEventCount: RangedParser[NullEventCount] = "[E-]" ^^^ NullEventCount()
  def eventLimit: RangedParser[EventLimit] = "[Emax]" ^^^ EventLimit()
  def bioTime: RangedParser[BioTime] = "[T]" ^^^ BioTime()
  def cpuTime: RangedParser[CpuTime] = "[Tsim]" ^^^ CpuTime()
  def bioTimeLimit: RangedParser[BioTimeLimit] = "[Tmax]" ^^^ BioTimeLimit()
  def tokenConcentration: RangedParser[TokenConcentration] = "|" ~> tokenName <~ "|" ^^ { TokenConcentration(_) }
  def patternConcentration: RangedParser[PatternConcentration] = "|" ~> kappaExpr <~ "|" ^^ { PatternConcentration(_) }

  def buildBinaryExpr(e1: NumExpr, l: List[BiNumOperator~NumExpr]): NumExpr = l match {
    case Nil => e1
    case (op~e2)::tail => {
      val e = op(e1, e2)
      e.sourceRange = SourceRange(e1.sourceRange.start, e2.sourceRange.end)
      buildBinaryExpr(e, tail)
    }
  }

  def unaryOp = unaryOpP1 | unaryOpP2 | unaryOpP3
  def unaryOpP1: RangedParser[UniNumOperator] =
    "-" ^^^ UnaryMinusOp()
  def unaryOpP2: RangedParser[UniNumOperator] =
    "[log]" ^^^ LogOp() |
    "[sin]" ^^^ SinOp() |
    "[cos]" ^^^ CosOp() |
    "[tan]" ^^^ TanOp() |
    "[sqrt]" ^^^ SqrtOp() |
    "[exp]" ^^^ ExpOp() |
    "[int]" ^^^ FloorOp()
  def unaryOpP3: RangedParser[UniNumOperator] =
    failure("no unary operators with priority 3")

  def prefixBinaryOp = prefixBinaryOpP1 | prefixBinaryOpP2 | prefixBinaryOpP3
  def prefixBinaryOpP1: RangedParser[BiNumOperator] =
    failure("no prefix binary operators with priority 1")
  def prefixBinaryOpP2: RangedParser[BiNumOperator] =
    "[min]" ^^^ MinOp() |
    "[max]" ^^^ MaxOp()
  def prefixBinaryOpP3: RangedParser[BiNumOperator] =
    failure("no binary prefix operators with priority 3")

  def binaryOpP1: RangedParser[BiNumOperator] =
    "+" ^^^ PlusOp() |
    "-" ^^^ MinusOp()

  def binaryOpP2: RangedParser[BiNumOperator] =
    "*" ^^^ TimesOp() |
    "/" ^^^ DivOp() |
    "[mod]" ^^^ ModOp()

  def binaryOpP3: RangedParser[BiNumOperator] =
    "^" ^^^ PowOp()


  def strExpr: RangedParser[StrExpr] =
    "<" ~> strExpr <~ ">" |
    strElem ~ "." ~ strExpr ^^ { case a~_~b => Concat(a, b) } |
    strElem

  def strElem: RangedParser[StrExpr] =
    stringLiteral ^^ { case s => StringLiteral(s) } |
    numExpr


  def boolExpr: RangedParser[BoolExpr] =
    andExpr ~ ("||" ~> andExpr).* ^^ { case first~rest => buildOrExpr(first, rest) }

  def andExpr: RangedParser[BoolExpr] =
    boolFactor ~ ("&&" ~> boolFactor).* ^^ { case first~rest => buildAndExpr(first, rest) }

  def boolFactor: RangedParser[BoolExpr] =
    "[not]" ~> boolExpr ^^ {case e => NotExpr(e) } |
    cmpExpr |
    "[true]" ^^^ True() |
    "[false]" ^^^ False() |
    "(" ~> boolExpr <~ ")"

  def cmpExpr: RangedParser[CmpExpr] =
    numExpr ~ "<" ~ numExpr ^^ { case a~_~b => LtExpr(a, b) } |
    numExpr ~ "<=" ~ numExpr ^^ { case a~_~b => LteExpr(a, b) } |
    numExpr ~ "=" ~ numExpr ^^ { case a~_~b => EqExpr(a, b) } |
    numExpr ~ ">=" ~ numExpr ^^ { case a~_~b => GteExpr(a, b) } |
    numExpr ~ ">" ~ numExpr ^^ { case a~_~b => GtExpr(a, b) }

  def buildOrExpr(first: BoolExpr, rest: List[BoolExpr]): BoolExpr = rest match {
    case Nil => first
    case h::tail => buildOrExpr(OrExpr(first, h), tail)
  }

  def buildAndExpr(first: BoolExpr, rest: List[BoolExpr]): BoolExpr = rest match {
    case Nil => first
    case h::tail => buildAndExpr(AndExpr(first, h), tail)
  }

  def tilde: RangedParser[Tilde] = "~" ^^^ Tilde()
  def exclamationMark: RangedParser[ExclamationMark] = "!" ^^^ ExclamationMark()
  def questionMark: RangedParser[QuestionMark] = "?" ^^^ QuestionMark()
  def underscore: RangedParser[Underscore] = """_\b""".r ^^^ Underscore()
  def atSign: RangedParser[AtSign] = "@" ^^^ AtSign()

  def doKw: RangedParser[DoKw] = """\bdo\b""".r ^^^ DoKw()
  def repeatKw: RangedParser[RepeatKw] = """\brepeat\b""".r ^^^ RepeatKw()
  def untilKw: RangedParser[UntilKw] = """\buntil\b""".r ^^^ UntilKw()

  def addCmd: RangedParser[AddCmd] = """\$ADD\b""".r ^^^ AddCmd()
  def delCmd: RangedParser[DelCmd] = """\$DEL\b""".r ^^^ DelCmd()
  def snapshotCmd: RangedParser[SnapshotCmd] = """\$SNAPSHOT\b""".r ^^^ SnapshotCmd()
  def stopCmd: RangedParser[StopCmd] = """\$STOP\b""".r ^^^ StopCmd()
  def fluxCmd: RangedParser[FluxCmd] = """\$FLUX\b""".r ^^^ FluxCmd()
  def trackCmd: RangedParser[TrackCmd] = """\$TRACK\b""".r ^^^ TrackCmd()
  def updateCmd: RangedParser[UpdateCmd] = """\$UPDATE\b""".r ^^^ UpdateCmd()
  def printCmd: RangedParser[PrintCmd] = """\$PRINT\b""".r ^^^ PrintCmd()
  def printfCmd: RangedParser[PrintfCmd] = """\$PRINTF\b""".r ^^^ PrintfCmd()
  def plotEntryCmd: RangedParser[PlotEntryCmd] = """\$PLOTENTRY\b""".r ^^^ PlotEntryCmd()
}